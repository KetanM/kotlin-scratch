import java.util.*
import java.util.stream.Stream
import kotlin.concurrent.thread

/**
 * Created by Stephen Fox on 9/12/2017.
 */

fun mergesort(array: IntArray): IntArray {
    val size: Int = array.size
    if (size == 1) return array
    if (size == 2) {
        Arrays.sort(array)
        return array
    }

    val left = mergesort(array.copyOfRange(0, size / 2))
    val right = mergesort(array.copyOfRange(size / 2, size))

    var l = 0 // left cursor
    var r = 0 // right cursor

    val merged = IntArray(size)

    var cursor: Int = 0
    while (cursor < merged.size && l < left.size && r < right.size) {
        if (left[l] < right[r]) {
            merged[cursor] = left[l]
            l++
        } else if (left[l] > right[r]) {
            merged[cursor] = right[r]
            r++
        }
        cursor++
    }

    while (l < left.size) merged[cursor++] = left[l++]
    while (r < right.size) merged[cursor++] = right[r++]

    return merged
}

fun main(args: Array<String>) {
    // doing sorting algorithms as a warm-up
    val rand = Random()
    val numbersToSort = Stream.generate { rand.nextInt(100) }.limit(10).mapToInt { it }.toArray()

    // time my sorting run
    val myThread = thread {
        val topTime = System.nanoTime()

        val sorted = mergesort(numbersToSort)

        println("Custom mergesort took ${(System.nanoTime() - topTime) / 1e9}s")
        println(Arrays.toString(sorted))
    }


    thread {
        val copiedNumbersToSort = numbersToSort.copyOf()
        val topTime = System.nanoTime()

        Arrays.sort(copiedNumbersToSort)

        val sortType = if (copiedNumbersToSort.size <= 286) "QuickSort" else "DualPivot Quicksort"

        println("${sortType} took ${(System.nanoTime() - topTime) / 1e9}s")
        println(Arrays.toString(copiedNumbersToSort))
    }

    myThread.join()
}
